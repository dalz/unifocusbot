(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Stdio
open Lib

let qatree = Questions.from_file "/srv/dokuwiki/data/pages/bot/domande.txt"
let config = Tg_front.{
      token = In_channel.read_all "/var/lib/telegram/unifocusbot.token";
      dict = Dict.from_file "/srv/dokuwiki/data/pages/bot/dizionario.txt";
      qatree;
      qaindex = Search.build_index qatree;
      groups = Groups.from_file "/srv/dokuwiki/data/pages/bot/gruppi_matricole.txt";
      admin_room = 96989037;
      equipe_room = -1001649261234;
      menus_cache = "/var/lib/telegram/menus.txt"; }

let () =
  Random.self_init ();
  Lwt_main.run (Tg_front.main config)

(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

type t =
  | Home
  | Browse of int list
  | Groups
  | MenuList | Menu of Canteen.t
  | Buildings | Rooms of Uniplan.building
  | Invalid of string

val decode : string -> t

val home : string
val groups : string

val menu_list : string
val menu : Canteen.t -> string

val buildings : string
val rooms : Uniplan.building -> string

val browse_top : string
val browse_down : string -> int -> string
val browse_up : string -> string

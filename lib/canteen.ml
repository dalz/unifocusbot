(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Cohttp_lwt
open Cohttp_lwt_unix

open Option.Monad_infix
let ( let* ) o f = Option.bind o ~f
let ( let+ ) o f = Option.map o ~f

type t = Betti | Cammeo | Martiri | Piagge | Praticelli | Rosellini
type menu = (t, string) Result.t * (string * string) list

let names = [Betti, "Betti"; Cammeo, "Cammeo"; Martiri, "Martiri";
             Piagge, "Piagge"; Praticelli, "Praticelli"; Rosellini, "Rosellini"]

let to_string canteen = List.Assoc.find_exn names canteen ~equal:Poly.equal
let of_string str =
  List.find_map names
    ~f:(fun (c, n) ->
      if String.(is_substring (lowercase str) ~substring:(lowercase n))
      then Some c else None)
  |> Result.of_option ~error:str

let url = Uri.of_string "https://www.dsu.toscana.it/i-menu"

let find html pattern pos = String.substr_index html ~pattern ~pos

let find_first html patterns pos =
  List.map patterns ~f:(fun pat -> find html pat pos)
  |> List.max_elt ~compare:(Option.compare (fun i j -> -(Int.compare i j)))
  |> Option.value_exn

let tag_text html si ei tag =
  let find = find html in
  let* tag_start = find ("<" ^ tag) si in
  let* text_start = find ">" tag_start >>| ((+) 1) in
  let* tend = find ("</" ^ tag) text_start in
  if tend > ei then None
  else Some (tag_start, String.sub html ~pos:text_start ~len:(tend - text_start))

let first_tag_text html si ei tags =
  List.map tags ~f:(tag_text html si ei)
  |> List.max_elt
       ~compare:(Option.compare (fun (i, _) (j, _) -> -(Int.compare i j)))
  |> Option.value_exn

let rec find_links_range html si ei res =
  let ( let* ) o f = match o with
    | Some x -> f x
    | None -> res
  in

  let* i, text = tag_text html si ei "a" in
  let* link_start = find html "href" i >>| ((+) 6) in
  let* link_end = find html "\"" link_start in
  let link = String.sub html ~pos:link_start ~len:(link_end - link_start) in
  let link = "https://www.dsu.toscana.it" ^ link in
  find_links_range html link_end ei ((text, link) :: res)

let rec find_menu_links html si ei res =
  match first_tag_text html si ei ["strong"; "b"] with
  | None -> res
  | Some (links_start, name) ->
     let links_end = Option.value (find_first html ["<strong>"; "<b>"; "<em>"]
                                     (links_start + 1))
                       ~default:ei
     in
     let links = find_links_range html links_start links_end [] in
     find_menu_links html links_end ei
       (if List.is_empty links then res else ((of_string name, links) :: res))

(** Throws an exception on request failure. [get_menus] can only be used by an
    admin, so there's no need for more robust error handling (as long as the
    caller handles the exn so that the program keeps running) *)
let get_menus () =
  let%lwt _, body = Client.get url in
  let%lwt html = Body.to_string body in
  Lwt.return (
      let find = find html in
      let* si = find "id=\"pisa\"" 0 >>= find "MENU' SELF SERVICE" in
      let+ ei = find "<h2" si in
      find_menu_links html si ei [])

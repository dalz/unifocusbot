(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Cohttp
open Cohttp_lwt
open Cohttp_lwt_unix

exception RequestError of exn
exception TgApiError of string * string

type update =
  | TextMessage of { chat_id : int; is_private : bool; text : string }
  | InlineQuery of { id : string; query : string }
  | CallbackQuery of { id : string; chat_id : int; msg_id : int; data : string }
  | Unknown

type button_content = BtnUrl of string | BtnData of string | SwitchInline

type message = (string * Yojson.Basic.t) list
type inline_query_result = Yojson.Basic.t
type button = Yojson.Basic.t
type keyboard = button list list


let parse_mode = "parse_mode", `String "Markdown"
let base = ref "token not provided"

let set_token tok = base := "https://api.telegram.org/bot" ^ tok ^ "/"

let api_req endpoint (p : Yojson.Basic.t) =
  let uri = Uri.of_string (!base ^ endpoint) in
  let headers = Header.of_list ["Content-type", "application/json"] in
  let body = Yojson.Basic.pretty_to_string p |> Body.of_string in
  let%lwt _, body = try%lwt Client.post ~headers ~body uri
                    with e -> raise (RequestError e)
  in
  Body.to_string body
  |> Lwt.map (fun b ->
         try
           let j = Yojson.Basic.from_string b in
           let open Yojson.Basic.Util in
           if member "ok" j |> to_bool then member "result" j
           else raise (TgApiError (endpoint, b))
         with Yojson.Json_error _ -> raise (TgApiError (endpoint, b)))

let chat_id json = Yojson.Basic.Util.(
    member "chat" json |> member "id" |> to_int)

let message_of_json json =
  let open Yojson.Basic.Util in
  try
    let is_private = member "chat" json |> member "type"
                     |> to_string |> String.equal "private"
    in
    TextMessage {
        chat_id = chat_id json;
        is_private;
        text = member "text" json |> to_string; }
  with _ -> Unknown

let inline_query_of_json json =
  let open Yojson.Basic.Util in
  try
    InlineQuery {
        id = member "id" json |> to_string;
        query = member "query" json |> to_string; }
  with _ -> Unknown

let callback_query_of_json json =
  let open Yojson.Basic.Util in
  try
    let msg = member "message" json in
    CallbackQuery {
        id = member "id" json |> to_string;
        chat_id = member "chat" msg |> member "id" |> to_int;
        msg_id =  member "message_id" msg |> to_int;
        data = member "data" json |> to_string; }
  with _ -> Unknown

let offset = ref 0

let update_of_json json =
  let open Yojson.Basic.Util in

  let rec if_has_field lst =
    match lst with
    | (field, f) :: tl ->
       (match member field json with
        | `Null -> if_has_field tl
        | x -> f x)
    | [] -> Unknown
  in

  let id = member "update_id" json |> to_int in
  offset := Int.max !offset (id + 1);

  if_has_field [
      "message", message_of_json;
      "inline_query", inline_query_of_json;
      "callback_query", callback_query_of_json]

let json_of_keyboard kb =
  ["reply_markup",
   `Assoc ["inline_keyboard", `List (List.map kb ~f:(fun r -> `List r))]]

let message ?keyboard ?(link_preview = true) text =
  ["text", `String text;
   "disable_web_page_preview", `Bool link_preview;
   parse_mode] @
    Option.value_map keyboard ~default:[] ~f:json_of_keyboard

let send_message chat_id msg =
  let p = ("chat_id", `Int chat_id) :: msg in
  let%lwt resp = api_req "sendMessage" (`Assoc p) in
  Yojson.Basic.Util.(member "message_id" resp |> to_int) |> Lwt.return

let edit_message chat_id msg_id msg =
  let p = ["chat_id", `Int chat_id; "message_id", `Int msg_id] @ msg in
  try%lwt api_req "editMessageText" (`Assoc p) |> Lwt.map ignore
  with TgApiError (_, b) as e ->
    if String.is_substring b ~substring:"are exactly the same as"
    then Lwt.return ()
    else raise e

let pin_message ?(unpin_all = false) chat_id msg_id =
  let p = ["chat_id", `Int chat_id] in
  let%lwt () =
    if unpin_all
    then api_req "unpinAllChatMessages" (`Assoc p) |> Lwt.map ignore
    else Lwt.return ()
  in
  let p = ("message_id", `Int msg_id) :: p in
  api_req "pinChatMessage" (`Assoc p) |> Lwt.map ignore

let send_document chat_id url_or_id =
  let p = `Assoc ["chat_id", `Int chat_id; "document", `String url_or_id] in
  let%lwt resp = api_req "sendDocument" p in
  let open Yojson.Basic.Util in
  member "document" resp |> member "file_id" |> to_string |> Lwt.return

let inline_query_result title description text =
  `Assoc [
    "type", `String "article";
    "id", `String Int64.(Random.int64 max_value |> to_string);
    "title", `String title;
    "description", `String description;
    "input_message_content", `Assoc ["message_text", `String text; parse_mode]]

let answer_inline_query id results =
  let p =`Assoc ["inline_query_id", `String id; "results", `List results] in
  api_req "answerInlineQuery" p |> Lwt.map ignore

let button text content =
  `Assoc ((match content with
           | BtnUrl url -> "url", `String url
           | BtnData data -> "callback_data", `String data
           | SwitchInline -> "switch_inline_query_current_chat", `String "")
          :: ["text", `String text])

let answer_callback_query ?text id =
  let p = ("callback_query_id", `String id)
          :: (match text with
              | Some s -> ["text", `String s]
              | None -> [])
  in api_req "answerCallbackQuery" (`Assoc p) |> Lwt.map ignore

let rec update_loop f exn_handler =
  let p = `Assoc (("timeout", `Int 60) ::
                    if !offset = 0 then []
                    else ["offset", `Int !offset])
  in

  let handle_batch = function
    | `List ups -> ups
                   |> List.map ~f:update_of_json
                   |> List.map ~f |> Lwt.join
    | _ -> failwith "getUpdates did not return an array"
  in

  try%lwt
    let%lwt ups = api_req "getUpdates" p in
    Lwt.dont_wait (fun () -> handle_batch ups) exn_handler;
    update_loop f exn_handler
  with e ->
    exn_handler e;
    update_loop f exn_handler


(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base

let set =
  Hash_set.of_list (module String)
    ["il"; "lo"; "la"; "i"; "gli"; "le"; "l";
     "un"; "uno"; "una";
     "di"; "del"; "dello"; "della"; "dei"; "degli"; "delle"; "dell";
     "a"; "al"; "allo"; "alla"; "ai"; "agli"; "alle";
     "da"; "dal"; "dallo"; "dalla"; "dai"; "dagli"; "dalle"; "dall";
     "in"; "nel"; "nello"; "nella"; "nei"; "negli"; "nelle"; "nell";
     "con"; "col"; "coi"; "coll";
     "su"; "sul"; "sullo"; "sulla"; "sui"; "sugli"; "sulle"; "sull";
     "per"; "tra"; "fra";
     "e"; "ed"; "che"; "ma"; "non"; "o"; "oppure"; "se" ]

(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Stdio

type t = { question : string; answer : string }
           [@@deriving compare, sexp_of, hash]

type tree =
  | Section of { name : string; desc : string; nodes : tree list }
  | Question of t

let qa_len_max = 4096 (* telegram api limit *)
let depth_max = 63 (* state encoding limits *)
let child_num_max = 95 (* ASCII ' ' to '~' *)

(* TODO doesn't belong here *)
let top_desc = "\nStiamo ancora completando questa sezione; se trovi qualche \
                errore puoi segnalarlo con \"/messaggio _testo_\"."
let empty_qatree = Section { name = "Elenco domande"; desc = top_desc; nodes = [] }

let rec qatree_insert tree path node =
  match path, tree with
  | [], Section { name; desc; nodes } ->
     Section { name; desc; nodes = node :: nodes }

  | hd :: tl, Section { name; desc; nodes } ->
     let found, nodes =
       List.fold_map nodes ~init:false ~f:(fun found -> function
           | Section { name; _ } as s ->
              if String.equal name hd
              then true, qatree_insert s tl node
              else found, s
           | Question _ as q -> found, q)
     in
     if found then Section { name; desc; nodes } else
       let s = (Section { name = hd; desc = ""; nodes = [] }) in
       Section { name; desc; nodes = qatree_insert s tl node :: nodes }

  | _ -> failwith "attempted insertion in Question node"

let from_file fname =
  let rec rev_and_validate ?(depth = 0) tree =
    if depth > depth_max then failwith "question tree too deep";
    match tree with
    | Section { name; desc; nodes } ->
       if List.length nodes > child_num_max then
         failwith (Printf.sprintf "section %s has too many children" name);
       let nodes = List.rev_map nodes ~f:(rev_and_validate ~depth:(depth + 1))
       in Section { name; desc; nodes }
    | Question qa as q ->
       if String.length qa.question + String.length qa.answer > qa_len_max then
         Util.pwarn "question longer than telegram limit (%s)" qa.question;
       q
  in

  let res =
    In_channel.with_file fname ~f:(fun chan ->
        let _ = In_channel.input_line chan in (* skip title *)
        let rec loop path cur res =
          let insert_cur () = match cur with
            | Some (Question _ as node) ->
               qatree_insert res path node
            | Some (Section _ as node) ->
               qatree_insert res (List.drop_last_exn path) node
            | None -> res
          in

          match Dokuwiki.read_line chan with
          | `Heading (text, level) ->
             (match level with
              | 1 -> failwith "unexpected level 1 heading"
              | 2 | 3 | 4 ->
                 let sect = Section { name = text; desc = ""; nodes = [] } in
                 let path' = List.take path (level - 2) @ [text] in
                 loop path' (Some sect) (insert_cur ())
              | 5 ->
                 let qa = Question { question = text; answer = "" } in
                 loop path (Some qa) (insert_cur ())
              | _ -> failwith "unreachable")

          | `Content s ->
             if List.is_empty path || String.is_prefix s ~prefix:">" then
               loop path cur res
             else
               let md = Dokuwiki.to_markdown s in
               let cur' = match cur with
                 | Some (Question qa) ->
                    Some (Question { qa with answer = qa.answer ^ "\n" ^ md })
                 | Some (Section { name; desc; nodes }) ->
                    Some (Section { name; desc = desc ^ "\n" ^ md; nodes })
                 | None -> failwith "answer without a question"
               in loop path cur' res

          | `Eof -> (insert_cur ())

        in loop [] None empty_qatree)
  in
  rev_and_validate res

let rec iter_qas ~f = function
  | Section { nodes; _ } -> List.iter ~f:(iter_qas ~f) nodes
  | Question qa -> f qa

let rec lookup_by_index_list tree lst =
  match tree, lst with
  | Section { nodes; _ }, hd :: tl ->
     Option.bind (List.nth nodes hd) ~f:(
         Fn.flip lookup_by_index_list tl)
  | _ -> Some tree

let map_level tree ~if_qa ~if_sect =
  match tree with
  | Question qa -> if_qa qa
  | Section { name; desc; nodes } ->
     let sub = List.map nodes ~f:(function
                   | Question { question; _ } -> question
                   | Section { name; _ } -> name)
     in if_sect name desc sub

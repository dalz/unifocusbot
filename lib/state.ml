(* Copyright (C) 2022-2023  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base

type t =
  | Home
  | Browse of int list
  | Groups
  | MenuList | Menu of Canteen.t
  | Buildings | Rooms of Uniplan.building
  | Invalid of string

let index_char_base = 32 (* ASCII ' ' *)

let encode_index i = Char.(of_int_exn (i + index_char_base) |> to_string)
let decode_index c =
  let i = Char.to_int c - index_char_base in
  if i >= 0 then Ok i else Error (Invalid "negative index")


let decode str =
  let prefix = String.get str 0 in (* data field can't be empty *)
  let data = String.drop_prefix str 1 in
  match prefix with
  | 'h' -> Home
  | 'g' -> Groups

  | 'm' ->
     if String.is_empty data then MenuList else
       let open Canteen in
       (match String.get data 0 with
        | 'b' -> Menu Betti | 'c' -> Menu Cammeo | 'm' -> Menu Martiri
        | 'P' -> Menu Piagge | 'p' -> Menu Praticelli | 'r' -> Menu Rosellini
        | c -> Invalid (Printf.sprintf "unrecognized canteen code '%c'" c))

  | 'r' ->
     if String.is_empty data then Buildings else
       let open Uniplan in
       (match String.get data 0 with
        | 'B' -> Rooms Ben | 'F' -> Rooms Fib | 'a' -> Rooms A | 'b' -> Rooms B
        | 'c' -> Rooms C | 'f' -> Rooms F | 'P' -> Rooms Pia | 'p' -> Rooms PN
        | 's' -> Rooms SR | 'S' -> Rooms Sap | 'v' -> Rooms Vet
        | c -> Invalid (Printf.sprintf "unrecognized building code '%c'" c))

  | 'q' ->
     String.fold_until data ~init:[] ~f:(fun ilst c ->
         match decode_index c with
         | Ok i -> Continue (i :: ilst)
         | Error s -> Stop s)
       ~finish:(fun ilst -> Browse (List.rev ilst))

  | c -> Invalid (Printf.sprintf "unrecognized prefix '%c'" c)

let home = "h"
let groups = "g"
let browse_top = "q"
let menu_list = "m"
let buildings = "r"

let menu canteen =
  let open Canteen in
  menu_list ^
    match canteen with
    | Betti -> "b" | Cammeo -> "c" | Martiri -> "m"
    | Piagge -> "P" | Praticelli -> "p" | Rosellini -> "r"

let rooms building =
  let open Uniplan in
  buildings ^
    match building with
    | Ben -> "B" | Fib -> "F" | A -> "a" | B -> "b" | C -> "c" | F -> "f"
    | Pia -> "P" | PN -> "p" | SR -> "s" | Sap -> "S" | Vet -> "v"

let browse_down cur_state i = cur_state ^ (encode_index i)

let browse_up cur_state =
  let up = String.drop_suffix cur_state 1 in
  if String.is_empty up then home else up

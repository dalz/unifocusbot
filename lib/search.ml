(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base

type index = (string, (Questions.t * int) list) Hashtbl.t

let preprocess str =
  let is_punct = String.contains "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~–" in
  let is_vowel = String.contains "aeiouAEIOU" in

  String.filter str ~f:(Fn.non (Char.equal '.'))
  |> String.map ~f:(fun c ->
         if is_punct c then ' '
         else Char.lowercase c)
  |> String.split_on_chars ~on:[' '; '\n']
  |> List.filter ~f:(fun s ->
         not (String.is_empty s || Hash_set.mem Stopwords.set s))
  |> List.map ~f:(fun s ->
         let l = String.length s in
         if l >= 4 && is_vowel (String.get s (l - 1))
         then String.drop_suffix s 1 else s)

let build_index qatree =
  let index = Hashtbl.create (module String) in
  let add_str qa str weight =
    preprocess str
    |> List.iter ~f:(fun word ->
           Hashtbl.update index word ~f:(function
               | Some tbl ->
                  Hashtbl.update tbl qa ~f:(fun o ->
                      Int.max (Option.value o ~default:0) weight);
                  tbl
               | None -> Hashtbl.of_alist_exn (module Questions) [qa, weight]))
  in
  Questions.iter_qas qatree ~f:(fun qa ->
      add_str qa qa.question 3;
      add_str qa qa.answer 1);

  Hashtbl.map index ~f:Hashtbl.to_alist

let results ?(max = 50) index query =
  let words = preprocess query in
  let scores = Hashtbl.create (module Questions) in

  List.iter words ~f:(fun w ->
      Option.iter (Hashtbl.find index w) ~f:(fun qas ->
          List.iter qas ~f:(fun (qa, weight) ->
              Hashtbl.update scores qa ~f:(function
                  | Some n -> n + weight
                  | None -> weight))));

  Hashtbl.to_alist scores
  |> List.sort ~compare:(fun (_, x) (_, y) -> Int.compare y x)
  |> Fn.flip List.take max
  |> List.map ~f:(fun (qa, _) -> qa)

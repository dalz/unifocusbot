(* Copyright (C) 2022-2023  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Core
open Cohttp
open Cohttp_lwt
open Cohttp_lwt_unix

type resv = { tstart : Time_float.Ofday.t; tend : Time_float.Ofday.t }
type availability = From of Time_float.Ofday.t | Until of Time_float.Ofday.t
type building = Ben | Fib | A | B | C | F | Pia | PN | SR | Sap | Vet

let building_calendar = function
  | Ben -> "63247fadac73c806bfa2e09a"
  | Fib -> "63223a029f080a0aab032afc"
  | A -> "63247d96e3772a0690e3bcb4"
  | B -> "63247e36ac73c806bfa2dfc2"
  | C -> "63247e5ee3772a0690e3bd51"
  | F -> "63247ea337746802ea1c1d4b"
  | Pia -> "63247758e3772a0690e3b9f3"
  | PN -> "63247c2237746802ea1c1cae"
  | SR -> "63247d5f75616d04046a0779"
  | Sap -> "63247af9ac73c806bfa2def2"
  | Vet -> "631e664188173200d821d046"

let string_of_building = function
  | Ben -> "Benedettine"
  | Fib -> "Fibonacci"
  | A -> "Ing A"
  | B -> "Ing B"
  | C -> "Ing C"
  | F -> "Ing F / Etruria"
  | Pia -> "Piagge"
  | PN -> "Porta Nuova"
  | SR -> "San Rossore"
  | Sap -> "Sapienza"
  | Vet -> "Veterinaria"

let get_day_ical_uri cal_id =
  let today = Util.today () |> Date.to_string in
  let day_start = today ^ "T00:00:00.000+01:00" in
  let day_end = today ^ "T23:59:59.999+01:00" in

  let uri = Uri.of_string "https://apache.prod.up.cineca.it/api/FiltriICal/creaFiltroICal" in
  let headers = Header.of_list ["Content-type", "application/json"] in
  let body = Yojson.Basic.to_string
               (`Assoc ["dataDa", `String day_start;
                        "dataA", `String day_end;
                        "clienteId", `String "628de8b9b63679f193b87046"; (* TODO *)
                        "linkCalendarioId", `String cal_id;
                        "dataScadenza", `String day_end])
             |> Body.of_string
  in

  let p =
    try%lwt Client.post ~headers ~body uri |> Lwt.map (fun x -> Ok x)
    with e -> Lwt.return (Error ("request to university planner failed: " ^ Exn.to_string e))
  in
  match%lwt p with
  | Error _ as e -> Lwt.return e
  | Ok (resp, body) ->
     Body.to_string body
     |> Lwt.map (fun b ->
            let open Yojson.Basic.Util in
            try
              let j = Yojson.Basic.from_string b in
              match member "error" j with
              | `Null ->
                 let uri =
                   "https://apache.prod.up.cineca.it/api/FiltriICal/impegniICal?id="
                   ^ (member "id" j |> to_string)
                 in Ok (Uri.of_string uri)
              | obj -> Error (member "message" obj |> to_string)
            with
              Yojson.Json_error _ ->
              Error (resp |> Response.status |> Code.code_of_status |> Int.to_string))

let resvs_of_ical str =
  let parse_date ds =
    try
      Time_float_unix.parse ds ~fmt:"%Y%m%dT%H%M%SZ" ~zone:Time_float.Zone.utc
      |> Time_float.to_ofday ~zone:Util.tz
      |> Option.some
    with _ ->
      Util.perr "couldn't parse ical date %s" ds;
      None
  in

  let res = Hashtbl.create (module String) in

  let add = function
    | Some room, Some tstart, Some tend ->
       Hashtbl.update res room
         ~f:(fun rs -> {tstart; tend} :: Option.value rs ~default:[])
    | None, None, None -> ()
    | _ -> Util.pwarn0 "incomplete event"
  in

  String.split_lines str
  |> List.fold ~init:(None, None, None) ~f:(
         fun ((room, tstart, tend) as acc) l ->
         match String.lsplit2 l ~on:':' with
         | None -> acc
         | Some (k, v) -> (
           match k with
           | "BEGIN" -> add acc; None, None, None
           | "DTSTART" -> room, parse_date v, tend
           | "DTEND" -> room, tstart, parse_date v
           | "LOCATION" ->
              let loc =
                match String.substr_index v ~pattern:" - " with
                | Some i -> String.prefix v i
                | None -> v
              in Some loc, tstart, tend
           | _ -> acc))
  |> add;
  Hashtbl.to_alist res
  |> List.sort ~compare:(fun (a, _) (b, _) -> String.compare b a)

let get_day_resvs =
  let cache = Hashtbl.create (module String) in

  let cache_get cal_id =
    Option.bind (Hashtbl.find cache cal_id) ~f:(
        fun (date, resvs) ->
        if Date.equal date (Util.today ())
        then Some resvs else None)
  in

  let cache_set cal_id resvs =
    Hashtbl.set cache ~key:cal_id ~data:(Util.today (), resvs)
  in

  fun cal_id ->
  match cache_get cal_id with
  | Some resvs -> Lwt.return (Ok resvs)
  | None ->
     let%lwt uri = get_day_ical_uri cal_id in
     match uri with
     | Error _ as e -> Lwt.return e
     | Ok uri ->
        let p =
          try%lwt Client.get uri |> Lwt.map (fun x -> Ok x)
          with e -> Lwt.return (Error ("request to university planner failed: " ^ Exn.to_string e))
        in
        match%lwt p with
        | Error _ as e -> Lwt.return e
        | Ok (resp, body) ->
           let%lwt body = Body.to_string body in
           let code = resp |> Response.status |> Code.code_of_status in
           Lwt.return
             (if code = 200 then
                let resvs = resvs_of_ical body in
                cache_set cal_id resvs;
                Ok resvs
              else
                Error (Printf.sprintf "GET %s -> %d" (Uri.to_string uri) code))

let available_rooms building =
  let aux resvs =
    let open Time_float in
    let min_free_mins = 20. in
    let now = now () |> to_ofday ~zone:Util.tz in
    let mdiff a b = Ofday.diff a b |> Span.to_min in
    List.fold resvs ~init:[] ~f:(fun acc (room, rs) ->
        match
          List.fold_until rs
            ~init:Ofday.(start_of_next_day, start_of_next_day)
            ~f:(fun (free_from, next_start) {tstart; tend} ->
              let free_from =
                if Float.(mdiff next_start tend >= min_free_mins)
                then tend else free_from
              in

              if Ofday.(<) tend now
              then Stop (Until next_start)

              else if Float.(mdiff tstart now < min_free_mins)
              then Stop (From free_from)

              else Continue (free_from, tstart))

            ~finish:(fun (_, next_start) -> Until next_start)

        with
        | Until t when Ofday.(t = start_of_next_day) -> acc
        | av -> (room, av) :: acc)
  in
  get_day_resvs (building_calendar building)
  |> Lwt.map (Result.map ~f:(fun r ->
                  aux r
                  |> List.partition_map ~f:(function
                         | room, Until t -> First (room, t)
                         | room, From t -> Second (room, t))))

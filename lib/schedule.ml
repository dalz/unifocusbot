(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Core

let next_day_time day time =
  let today = Util.today () in
  let now = Time_float.(now () |> to_ofday ~zone:Util.tz) in
  let d =
    if Day_of_week.equal day (Date.day_of_week today) && Time_float.Ofday.(<) now time
    then Date.add_days today (-1)
    else today
  in
  let date = Date.first_strictly_after ~on:day d in
  Time_float.of_date_ofday ~zone:Util.tz date time

let rec weekly f day time =
  let t = next_day_time day time in
  let%lwt () = Lwt_unix.sleep Time_float.(diff t (now ()) |> Span.to_sec) in
  let%lwt () = try%lwt f () with e ->
                 Util.perr "error in scheduled function: %s" (Exn.to_string e);
                 Lwt.return ()
  in weekly f day time

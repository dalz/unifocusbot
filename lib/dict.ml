(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Stdio

type t = (string, string) Hashtbl.t

(** format: key0, key1: def
    inside <file>...</file> *)
let from_file fname =
  let dict = Hashtbl.create (module String) in
  In_channel.with_file fname ~f:(
      Dokuwiki.fold_file_block_lines ~init:() ~f:(fun () line ->
          let open String in
          if not (is_empty line || Char.equal (get line 0) '#') then
            let keys, def = lsplit2_exn line ~on:':' in
            let def = strip def in
            let keys = split keys ~on:','
                       |> List.map ~f:(Fn.compose strip lowercase)
            in List.iter keys ~f:(fun k ->
                   Hashtbl.add_exn dict ~key:k ~data:def)));
  dict

let find dict key = Hashtbl.find dict (String.lowercase key)

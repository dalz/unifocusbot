(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Stdio

type t = (string * string) list

let from_file fname =
  In_channel.with_file fname ~f:(fun chan ->
      Dokuwiki.fold_file_block_lines chan ~init:[] ~f:(fun lst line ->
          let open String in
          if not (is_empty line || Char.equal (get line 0) '#') then
           let name, url = lsplit2_exn line ~on:':' in
           (strip name, strip url) :: lst
          else lst)
      |> List.rev)

(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base

type t = { question : string; answer : string }
type tree

val from_file : string -> tree
val iter_qas : f:(t -> unit) -> tree -> unit
val lookup_by_index_list : tree -> int list -> tree option
val map_level : tree
                 -> if_qa:(t -> 'a)
                 -> if_sect:(string -> string -> string list -> 'a)
                 -> 'a

include Hashtbl.Key.S with type t := t

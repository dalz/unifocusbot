(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base

exception RequestError of exn
exception TgApiError of string * string

val set_token : string -> unit

type update =
  | TextMessage of { chat_id : int; is_private : bool; text : string }
  | InlineQuery of { id : string; query : string }
  | CallbackQuery of { id : string; chat_id : int; msg_id : int; data : string }
  | Unknown

type inline_query_result

val inline_query_result : string -> string -> string -> inline_query_result
val answer_inline_query : string -> inline_query_result list -> unit Lwt.t

type button
type button_content = BtnUrl of string | BtnData of string | SwitchInline
type keyboard = button list list

val button : string -> button_content -> button
val answer_callback_query : ?text:string -> string -> unit Lwt.t

type message

val message : ?keyboard:keyboard -> ?link_preview:bool -> string -> message
val send_message : int -> message -> int Lwt.t
val edit_message : int -> int -> message -> unit Lwt.t
val pin_message : ?unpin_all:bool -> int -> int -> unit Lwt.t
val send_document : int -> string -> string Lwt.t

val update_loop : (update -> unit Lwt.t) -> (exn -> unit) -> unit Lwt.t

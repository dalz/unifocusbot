(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Core
open Stdio.Out_channel

let err () = eprintf "[%s] ERROR: " (Time_float.to_string_utc (Time_float.now ()))
let warn () = eprintf "[%s] WARNING: " (Time_float.to_string_utc (Time_float.now ()))

let perr0 msg = err (); eprintf "%s" msg; eprintf "\n"; flush stderr
let perr fmt v = err (); eprintf fmt v; eprintf "\n"; flush stderr
let perr2 fmt u v = err (); eprintf fmt u v; eprintf "\n"; flush stderr
let pwarn0 msg = warn (); eprintf "%s" msg; eprintf "\n"; flush stderr
let pwarn fmt v = warn (); eprintf fmt v; eprintf "\n"; flush stderr
let pwarn2 fmt u v = warn (); eprintf fmt u v; eprintf "\n"; flush stderr

let tz = Time_float.Zone.of_utc_offset ~hours:2
let today () = Date.today ~zone:tz

let string_of_ofday t =
  let open Time_float in
  let Span.Parts.{hr; min; _} =
    Ofday.to_span_since_start_of_day t |> Span.to_parts
  in Printf.sprintf "%d:%02d" hr min

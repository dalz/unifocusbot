(* (\* Copyright (C) 2022-2023  dalz *)

(*    This file is part of unifocusbot. *)

(*    This program is free software: you can redistribute it and/or modify *)
(*    it under the terms of the GNU Affero General Public License as published by *)
(*    the Free Software Foundation, either version 3 of the License, or *)
(*    (at your option) any later version. *)

(*    This program is distributed in the hope that it will be useful, *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the *)
(*    GNU Affero General Public License for more details. *)

(*    You should have received a copy of the GNU Affero General Public License *)
(*    along with this program.  If not, see <https://www.gnu.org/licenses/>. *\) *)

open Core
open Stdio

type config =
  { token : string;
    dict : Dict.t;
    qatree : Questions.tree;
    qaindex : Search.index;
    groups : Groups.t;
    admin_room : int;
    equipe_room : int;
    menus_cache : string; }

let home_btn = Tg_api.(button "🏠  Home" (BtnData State.home))

let private_start_msg =
  Tg_api.message
    "*Benvenuto/a nel bot del* [progetto Focus](https://t.me/focus_unipi)*!*\n\n\
    Puoi cercare tra le domande scrivendo \"@unifocusbot _parole chiave_\", \
    oppure sfogliarle premendo su *Elenco domande*.\n\n\
    Con la stessa modalità puoi trovare il significato di alcune sigle \
    relative all'università, per esempio \"@unifocusbot PN\".\n\n\
    Se hai qualche dubbio o suggerimento puoi contattarci anonimamente inviando \
    \"/messaggio _testo_\" in questa chat, oppure scriverci sul \
    [nostro gruppo](https://t.me/focusunipi)."
    ~keyboard:[[Tg_api.button "Cerca tra le domande" SwitchInline];
               [Tg_api.button "Elenco domande" (BtnData State.browse_top)];
               [Tg_api.button "Aule libere" (BtnData State.buildings)];
               [Tg_api.button "Menù mense" (BtnData State.menu_list)];
               [Tg_api.button "Gruppi matricole" (BtnUrl "https://linktr.ee/faby66") (* (BtnData State.groups) *)]]

let group_start_msg =
  Tg_api.message
    "Ciao, questo è il bot del [progetto Focus](https://t.me/focus_unipi)!\n\n\
     Puoi cercare tra le domande in qualsiasi chat scrivendo \"@unifocusbot _domanda_\".\n\n\
     Avvia il bot in chat privata per altre funzionalità (es. menù mense e gruppi matricole)."
    ~keyboard:[[Tg_api.button "Cerca tra le domande" SwitchInline];
               [Tg_api.button "Vai in chat privata" (BtnUrl "t.me/unifocusbot")]]

let menu_list_msg =
  let open Tg_api in
  message "*Menù mense*"
    ~keyboard:[[home_btn];
               [button "Betti" (BtnData (State.menu Canteen.Betti));
                button "Cammeo" (BtnData (State.menu Canteen.Cammeo))];
               [button "Martiri" (BtnData (State.menu Canteen.Martiri));
                button "Piagge" (BtnData (State.menu Canteen.Piagge))];
               [button "Praticelli" (BtnData (State.menu Canteen.Praticelli));
                button "Rosellini" (BtnData (State.menu Canteen.Rosellini))]]

let building_select_msg =
  let open Tg_api in
  let open Uniplan in
  let make_btn b = button (string_of_building b) (BtnData (State.rooms b)) in
  message "*Aule libere*\nSeleziona un polo didattico."
    ~keyboard:([home_btn] ::
                 (List.map ~f:(List.map ~f:make_btn)
                    [[Ben; Fib]; [A; B]; [C; F]; [Pia; PN]; [SR; Sap]; [Vet]]))

let available_rooms_msg (free, busy) b =
  let aux lst bullet =
    List.map lst ~f:(fun (r, t) ->
        Printf.sprintf "%s *%s* fino alle %s" bullet r (Util.string_of_ofday t))
    |> String.concat ~sep:"\n"
  in

  let text =
    Printf.sprintf
      "*Aule libere — %s*\n\nUn'aula è considerata libera se è inutilizzata per almeno 20 minuti. \
       *Le aule non elencate sono libere per il resto della giornata*.\n\n\
       *Atttualmente libere*\n%s\n\n*Attualmente occupate*\n%s\n\n_Ultimo aggioramento: %s_"
      (Uniplan.string_of_building b) (aux free "🟢") (aux busy "🔴")
      Time_float.(now () |> to_ofday ~zone:Util.tz |> Util.string_of_ofday)
  in
  Tg_api.message
    ~keyboard:(Tg_api.([[button "⬆️  Su" (BtnData State.buildings); home_btn];
                        [button "🔄  Aggiorna" (BtnData (State.rooms b))]]))
    text

let equipe_fwd_ack_msg =
  Tg_api.message
    "Grazie per averci contattato! Ricorda che i messaggi che riceviamo sono \
     anonimi, quindi manda anche un modo per raggiungerti (es. username \
     Telegram) se vuoi una risposta, oppure scrivici sul \
     [nostro gruppo](https://t.me/focusunipi)"

let menu_unavailable_msg_text : ('a -> 'b, unit, string) Stdlib.format =
    "Il menù per mensa %s non è disponibile. Potrebbe non essere stato \
     caricato sul sito del DSU, o forse la mensa è chiusa; puoi controllare \
     [su questa pagina](https://www.dsu.toscana.it/i-menu#pisa)."

let groups_msg_text =
  "*Link gruppi matricole*\n\
   Se conosci un gruppo che non è presente nella lista, faccelo sapere \
   (es. con \"/messaggio _link_\")"

let iqr_of_qa Questions.{ question; answer } =
  let desc_len_max = 50 in
  let desc =
    let s = String.tr ~target:'\n' ~replacement:' ' answer in
    if String.length s > desc_len_max then
      String.rfindi s ~pos:(desc_len_max - 1) ~f:(fun _ -> Char.is_whitespace)
      |> Option.value ~default:(desc_len_max - 1)
      |> String.prefix s |> Fn.flip (^) "…"
    else s
  in
  let msg = Printf.sprintf "*%s*%s" question answer in
  Tg_api.inline_query_result question desc msg

let iqr_of_dict_query dict query =
  Option.map (Dict.find dict query) ~f:(fun def ->
      let msg = Printf.sprintf "*%s*: %s" query def in
      Tg_api.inline_query_result query def msg)

let qa_nav_btn cur_state =
  Tg_api.([[button "⬆️  Su" (BtnData (State.browse_up cur_state)); home_btn]])

let show_qa cur_state Questions.{question; answer} =
  let text = Printf.sprintf "*%s*%s" question answer in
  Tg_api.message text ~keyboard:(qa_nav_btn cur_state)

let show_sect cur_state name desc nodes =
  let open Tg_api in
  let sect_btns, last_row, _ =
    List.foldi nodes ~init:([], [], 0) ~f:(fun i (res, cur, len) s ->
        let btn = button s (BtnData (State.browse_down cur_state i)) in
        let slen = String.length s in
        let len = len + slen in
        if len < 30 then res, cur @ [btn], len
        else cur :: res, [btn], slen) in
  let sect_btns = List.rev (last_row :: sect_btns) in
  let keyboard = qa_nav_btn cur_state @ sect_btns in
  let msg = Printf.sprintf "*%s*%s" name desc in
  message msg ~keyboard

let new_menus = ref []
let cur_menus = ref []

let save_menus fname =
  let ch = Out_channel.create fname in
  let%lwt _ =
    Lwt_list.iter_s (fun (cant, ids) ->
        let%lwt ids = Lwt_list.map_p Fn.id ids in
        String.concat (Canteen.to_string cant :: ids) ~sep:","
        |> Out_channel.fprintf ch "%s\n";
        Lwt.return ())
      !cur_menus
  in Out_channel.close ch; Lwt.return ()

let load_menus fname =
  cur_menus :=
    In_channel.with_file fname ~f:(fun ch ->
        In_channel.fold_lines ch ~init:[] ~f:(fun menus line ->
            match String.split line ~on:',' with
            | name :: links ->
               if List.is_empty links
               then (Util.perr "invalid line in menus file: %s" line; menus)
               else (Canteen.of_string name |> Result.ok_or_failwith,
                     List.map links ~f:Lwt.return) :: menus
            | [] -> failwith "unreachable"))

let get_menus_ask_confirm chat_id =
  let string_of_links_list links i =
    List.fold links ~init:(i, "", [])
      ~f:(fun (i, s, links') (text, link) ->
        i + 1,
        Printf.sprintf "%s\n%d. %s: %s" s i text link,
        (i, text, link) :: links')
  in

  let string_of_menus menus =
    let _, s, menus' =
      List.fold menus ~init:(0, "", [])
        ~f:(fun (i, s, menus') (cant, links) ->
          let name = match cant with
            | Ok c -> Canteen.to_string c
            | Error s -> "non riconosciuta: " ^ s
          in
          let i, ls, links = string_of_links_list links i in
          i, Printf.sprintf "%s\n\n*%s*\n%s" s name ls, (cant, links) :: menus')
    in s, menus'
  in

  let%lwt text =
    match%lwt Canteen.get_menus () with
    | Some menus ->
       let text, menus' = string_of_menus menus in
       new_menus := menus';
       Lwt.return text
    | None -> Lwt.return "errore"
  in
  let text = String.substr_replace_all text ~pattern:"_" ~with_:"\\_" in (* TODO brutto, per i link *)
  Tg_api.(send_message chat_id (message text)) |> Lwt.map ignore

let accept_menus menus_cache chat_id text =
  let accepted =
    String.split text ~on:' '
    |> Fn.flip List.drop 1
    |> List.map ~f:Int.of_string
  in

  cur_menus :=
    List.filter_map !new_menus ~f:(fun (cant, links) ->
        match cant with
        | Error _ -> None
        | Ok cant ->
           let links' =
             List.filter_map links ~f:(fun (i, _, link) ->
                 if List.mem accepted i ~equal:(=)
                 then Some (Tg_api.send_document chat_id link)
                 else None)
           in
           if List.is_empty links' then None else Some (cant, links'));

  new_menus := [];
  save_menus menus_cache

let send_start_message chat_id is_private =
  if is_private then
    let%lwt msg_id = Tg_api.send_message chat_id private_start_msg
    in Tg_api.pin_message ~unpin_all:true chat_id msg_id
  else
    Tg_api.send_message chat_id group_start_msg |> Lwt.map ignore

let forward_to_equipe equipe_room chat_id text =
  let open Tg_api in
  (if String.is_suffix text ~suffix:"/messaggio" then
     send_message chat_id
       (message "Il messaggio è vuoto! Usa \"/messaggio _testo_\".")
   else
     let%lwt _ = send_message chat_id equipe_fwd_ack_msg in
     send_message equipe_room (message text))
  |> Lwt.map ignore

let message_handler config chat_id is_private text =
  let { admin_room; equipe_room; menus_cache; _} = config in
  let rec on_cmd = function
    | (cmd, f) :: tl ->
       if String.is_prefix ~prefix:cmd text then f () else on_cmd tl
    | [] -> Lwt.return ()
  in

  let if_admin f = if chat_id = admin_room then f () else Lwt.return () in

  on_cmd [
      "/start", (fun () ->
        send_start_message chat_id is_private);

      "/messaggio", (fun () ->
        if is_private
        then forward_to_equipe equipe_room chat_id text
        else Lwt.return ());

      "/getmenus", (fun () ->
        if_admin (fun () -> get_menus_ask_confirm chat_id));

      "/accept", (fun () ->
        if_admin (fun () -> accept_menus menus_cache chat_id text));
    ]

let callback_query_handler qatree groups_msg id chat_id msg_id data =
  let open Tg_api in

  let err_msg_and_home () =
    let%lwt _ = answer_callback_query id ~text:"errore, prova di nuovo"
    in edit_message chat_id msg_id private_start_msg
  in

  match State.decode data with
   | Home -> edit_message chat_id msg_id private_start_msg

   | MenuList -> edit_message chat_id msg_id menu_list_msg
   | Menu cant ->
      (match List.Assoc.find !cur_menus cant ~equal:Poly.equal with
       | Some file_ids ->
          let%lwt () =
            Lwt_list.iter_p (fun id ->
                Lwt.bind id (fun id ->
                    send_document chat_id id |> Lwt.map ignore))
              file_ids
          in

          (* "self-repair" if the message structure changes *)
          let%lwt () = edit_message chat_id msg_id menu_list_msg in
          answer_callback_query id

       | None ->
          send_message chat_id
            (Printf.sprintf menu_unavailable_msg_text (Canteen.to_string cant)
             |> message)
          |> Lwt.map ignore)

   | Buildings -> edit_message chat_id msg_id building_select_msg
   | Rooms b ->
      let%lwt ar = Uniplan.available_rooms b in
      (match ar with
       | Error msg -> Util.perr0 msg; err_msg_and_home ()
       | Ok ar -> available_rooms_msg ar b |> edit_message chat_id msg_id)

   | Browse ilst ->
      (match Questions.lookup_by_index_list qatree ilst with
       | Some t ->
          Questions.map_level t ~if_qa:(show_qa data) ~if_sect:(show_sect data)
          |> edit_message chat_id msg_id

       | None ->
          Util.perr "invalid index list [%s]"
            (List.map ilst ~f:Int.to_string |> String.concat ~sep:", ");

          err_msg_and_home ())

   | Groups -> edit_message chat_id msg_id groups_msg

   | Invalid s ->
      Util.perr2 "invalid state '%s': %s" data s;
      err_msg_and_home ()

let handler ({dict; qatree; qaindex; groups; _} as config) =
  let open Tg_api in

  let groups_msg =
    message groups_msg_text ~keyboard:(
        [home_btn] ::
        List.map groups ~f:(fun (name, link) -> [button name (BtnUrl link)]))
  in

  function
  | TextMessage {chat_id; is_private; text} ->
     message_handler config chat_id is_private text

  | InlineQuery {id; query} ->
     let qas = Search.results qaindex query |> List.map ~f:iqr_of_qa in
     let def = iqr_of_dict_query dict query in
     answer_inline_query id
       (match def with
       | Some def -> def :: qas
       | None -> qas)

  | CallbackQuery {id; chat_id; msg_id; data} ->
     callback_query_handler qatree groups_msg id chat_id msg_id data

  | Unknown -> Lwt.return ()

let main config =
  Tg_api.set_token config.token;

  (try load_menus config.menus_cache with e ->
     Util.pwarn "couldn't load menus file: %s" (Exn.to_string e));
  Lwt.dont_wait (fun () ->
      Schedule.weekly (fun () -> get_menus_ask_confirm config.admin_room)
        Day_of_week.Sun (Time_float.Ofday.create ~hr:20 ~min:15 ()))
    (fun e -> Util.perr "error in scheduled task: %s" (Exn.to_string e));

  Tg_api.update_loop (handler config) (function
      | Tg_api.RequestError e ->
         Util.perr "request to api.telegram.org failed: %s" (Exn.to_string e);
         Core_unix.sleep 3; (* synchronous *)

      | Tg_api.TgApiError (ep, resp) ->
         Util.perr2 "api endpoint %s returned %s" ep resp;

      | e -> Util.perr "unhandled exception: %s" (Exn.to_string e))

(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Core
open Stdio

val perr0 : string -> unit
val perr : ('a -> unit, Out_channel.t, unit) format -> 'a -> unit
val perr2 : ('a -> 'b -> unit, Out_channel.t, unit) format -> 'a -> 'b -> unit
val pwarn0 : string -> unit
val pwarn : ('a -> unit, Out_channel.t, unit) format -> 'a -> unit
val pwarn2 : ('a -> 'b -> unit, Out_channel.t, unit) format -> 'a -> 'b -> unit

val tz : Time_float.Zone.t
val today : unit -> Date.t
val string_of_ofday : Time_float.Ofday.t -> string

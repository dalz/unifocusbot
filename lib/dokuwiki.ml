(* Copyright (C) 2022  dalz

   This file is part of unifocusbot.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. *)

open Base
open Stdio

let to_markdown =
  let italics_re = Str.regexp {|\(^\|[^:]\)//|} in
  let link_re = Str.regexp {|\[\[\([^|]+\)|\([^]]+\)\]\]|} in
  let asterisk_re = Str.regexp {|\(^\|[^*]\)\*\([^*]\|$\)|} in
  fun s ->
  Str.global_replace asterisk_re {|\1\*\2|} s
  |> String.substr_replace_all ~pattern:"**" ~with_:"*"
  |> Str.global_replace italics_re "\\1_"
  |> Str.global_replace link_re "[\\2](\\1)"

let read_line chan =
  match In_channel.input_line chan with
  | Some l ->
     let hd_lvl = 7 - String.common_prefix2_length l "======" in
     if hd_lvl >= 1 && hd_lvl <= 5 then
       let s = String.strip ~drop:(function '=' | ' ' -> true | _ -> false) l in
       `Heading (s, hd_lvl)
     else
       `Content l
  | None -> `Eof

let fold_file_block_lines chan ~init ~f =
  let _, res =
    In_channel.fold_lines chan ~init:(true, init)
      ~f:(fun (skip, acc) line ->
        if String.is_prefix line ~prefix:"<file>" then false, acc
        else if String.is_prefix line ~prefix:"</file>" then true, acc
        else if skip then skip, acc
        else skip, f acc line)
  in res

#!/bin/sh
set -e

host=root@alsd.eu
dest=/var/lib/telegram/unifocusbot/

rsync -avz --delete bin lib dune-project unifocusbot.opam $host:$dest
ssh $host chown -R telegram:telegram $dest
scp openrc/unifocusbot $host:/etc/init.d/

if [ "$1" = "-i" ]; then
    echo opam install $dest -y --deps-only | ssh $host su telegram -s /bin/sh
fi

ssh $host <<EOF
su telegram -s /bin/sh sh -c 'cd $dest; eval \$(opam env); dune build bin'
rc-service unifocusbot restart
EOF


